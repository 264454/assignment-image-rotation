#include "bmp.h"
#include <stdbool.h>

uint64_t padding_count(uint64_t width) {
    uint64_t size = width * sizeof(struct pixel);
    if(size % 4 == 0) return 0;
	uint64_t padding = 4 - (size % 4);
	return padding;
}

uint64_t data_size(struct image* img) {
	uint64_t row_size = sizeof(struct pixel) * img->width + padding_count(img->width);
	uint64_t data_size = row_size * img->height;
	return data_size;
}

uint64_t bmp_size(struct image* img){
	uint64_t d_size = data_size(img);
	uint64_t header_size = sizeof(struct bmp_header);
	return header_size + d_size;
}

struct bmp_header new_bmp_header(struct image* img) {
	return (struct bmp_header) {
		.bfType = 0x4D42,
		.bfileSize = bmp_size(img),
		.bfReserved = 0,
		.bOffBits = sizeof(struct bmp_header),
		.biSize = 40, //sizeof(Windows' BITMAPINFOHEADER)
		.biWidth = img->width,
		.biHeight = img->height,
		.biPlanes = 1, //Must be 1, artifact of the past
		.biBitCount = sizeof(struct pixel) * 8, //bits per pixel
		.biCompression = 0, //We do not compress this one
		.biSizeImage = data_size(img),
		.biXPelsPerMeter = 0, //ignoring resolution
		.biYPelsPerMeter = 0,
		.biClrUsed = 0, // default number of colors
		.biClrImportant = 0 // every color is important so 0
	};
}
