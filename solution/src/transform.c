
#include "transform.h"

struct image rotate_90_deg(struct image img) {
	uint64_t width = img.width;
	uint64_t height = img.height;
	uint64_t width_rotated = img.height;
	uint64_t height_rotated = img.width;
	struct image rotated = new_image(width_rotated, height_rotated);
	for(uint64_t i = 0; i < height; i++) {
		for(uint64_t j = 0; j < width; j++) {
			uint64_t j_rotated = height - i - 1;
			uint64_t i_rotated = j;
			rotated.data[i_rotated * width_rotated + j_rotated] = img.data[j + i * width];
		}
	}
	return rotated;
}
