#include "img.h"
#include <stdlib.h>

struct image new_image(const uint64_t width, const uint64_t height) {
	return (struct image) {
		.width = width,
		.height = height,
		.data = (struct pixel*) calloc(width * height, sizeof(struct pixel))
	};
}

void clear_img(const struct image* img) {
	free(img->data);
}
