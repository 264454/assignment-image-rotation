#include "write.h"
#include "bmp.h"
#include <stdbool.h>
#include <stdio.h>

static bool write_header(FILE* file, struct bmp_header* header) {
	return fwrite(header, sizeof(struct bmp_header), 1, file);
}

static bool write_pixel(FILE* file, struct pixel* data, uint64_t width) {
  return fwrite(data, sizeof(struct pixel), width, file) == width;
}

static bool write_padding(FILE* file, uint64_t width) {
  uint64_t zero = 0;
  puts("Ow");
  return fwrite(&zero, padding_count(width), 1, file);
}

static bool write_data(FILE* file, struct image* img) {	
  uint64_t width = img->width;
  uint64_t height = img->height;
  struct pixel* pixels = img->data;
  for(uint64_t i = 0; i < height; i++) {
    if(!write_pixel(file, pixels, width)) return false;
    if(padding_count(width)){
    	if(!write_padding(file, width)) return false;
    }
    pixels += width;
  }
  return true;
}

enum write_result img_to_bmp(FILE* file, struct image* img) {
	struct bmp_header h = new_bmp_header(img);
	if(!write_header(file, &h)) return WRITE_HEADER_ERROR;
	fseek(file, h.bOffBits, SEEK_SET);
	if(!write_data(file, img)) return WRITE_DATA_ERROR;
	return WRITE_OK;
}
