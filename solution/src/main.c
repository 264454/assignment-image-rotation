#include "img.h"
#include "read.h"
#include "transform.h"
#include "write.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if(argc < 3) {
	    puts("Argument missed\n");
	    return -1;
    }
    char* in = argv[1];
    char* out = argv[2];
    struct image img;
    FILE* in_file = fopen(in, "r");
    enum read_result res_r = bmp_to_img(in_file, &img);
    fclose(in_file);
    if(res_r != 0){
	    printf("Error caused by reading file '%s': %d\n", in, res_r);
	    return -1;
    }
    struct image result;
    result = rotate_90_deg(img);
    clear_img(&img);
    FILE* out_file;
    out_file = fopen(out, "w");
    enum write_result res_w = img_to_bmp(out_file, &result);
    clear_img(&result);
    fclose(out_file);
    if(res_w != 0) {
	    printf("Error caused by writing file '%s': %d\n", out, res_w);
	    return -1;
    }
    printf("Result in '%s'\n", out);
    return 0;
}
