#include "read.h"
#include "bmp.h"
#include <stdbool.h>

static bool read_header(FILE* file, struct bmp_header* header) {
	return fread(header, sizeof(struct bmp_header), 1, file);
}

static bool read_pixel(FILE* file, struct pixel* pixel, uint64_t width) {
	return fread(pixel, sizeof(struct pixel), width, file);
}

static bool read_data(FILE* file, struct bmp_header* header, struct image* img) {
    uint64_t padding = padding_count(header->biWidth);
    struct pixel* pixels;
    *img = new_image(header->biWidth, header->biHeight);
    pixels = img->data;
    for(uint64_t i = 0; i < img->height; i++) {
        if(!read_pixel(file, pixels, img->width)) return false;
        if(fseek(file, padding, SEEK_CUR) != 0) return false;
        pixels += img->width;
    }
    return true;
}

enum read_result bmp_to_img(FILE* file, struct image* img) {
	struct bmp_header header;
	if(!read_header(file, &header)) return READ_HEADER_ERROR;
	if(!read_data(file, &header, img)) return READ_DATA_ERROR;
	return READ_OK;
}
