#include "img.h"
//#include <stdint.h>
#include <stdio.h>

enum write_result {
	WRITE_OK = 0,
	WRITE_HEADER_ERROR,
	WRITE_DATA_ERROR
};

enum write_result img_to_bmp( FILE* file, struct image* img);
