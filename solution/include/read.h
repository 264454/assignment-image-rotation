#include "img.h"
//#include <stdint.h>
#include <stdio.h>

enum read_result {
	READ_OK = 0,
	READ_HEADER_ERROR,
	READ_DATA_ERROR
};

enum read_result bmp_to_img( FILE* file, struct image* img);
